package com.telerikacademy.oop.agency.core;

import com.telerikacademy.oop.agency.commands.contracts.Command;
import com.telerikacademy.oop.agency.core.contracts.*;
import com.telerikacademy.oop.agency.core.factories.AgencyFactoryImpl;
import com.telerikacademy.oop.agency.core.factories.CommandFactoryImpl;
import com.telerikacademy.oop.agency.core.providers.CommandParserImpl;
import com.telerikacademy.oop.agency.core.providers.ConsoleReader;
import com.telerikacademy.oop.agency.core.providers.ConsoleWriter;

import java.util.List;

public class AgencyEngineImpl implements Engine {

    private static final String TERMINATION_COMMAND = "Exit";

    private final Reader reader;
    private final Writer writer;
    private final AgencyFactory agencyFactory;
    private final CommandFactory commandFactory;
    private final CommandParser commandParser;
    private final AgencyRepository agencyRepository;

    public AgencyEngineImpl() {
        this.reader = new ConsoleReader();
        this.writer = new ConsoleWriter();
        this.agencyFactory = new AgencyFactoryImpl();
        this.commandFactory = new CommandFactoryImpl();
        this.commandParser = new CommandParserImpl();
        this.agencyRepository = new AgencyRepositoryImpl();
    }

    public void start() {
        while (true) {
            try {
                String commandAsString = reader.readLine();
                if (commandAsString.equalsIgnoreCase(TERMINATION_COMMAND)) {
                    break;
                }
                processCommand(commandAsString);

            } catch (Exception ex) {
                writer.writeLine(ex.getMessage() != null && !ex.getMessage().isEmpty() ? ex.getMessage() : ex.toString());
            }
        }
    }

    private void processCommand(String commandAsString) {
        if (commandAsString == null || commandAsString.trim().equals("")) {
            throw new IllegalArgumentException("Command cannot be null or empty.");
        }

        try {
            String commandName = commandParser.parseCommand(commandAsString);
            Command command = commandFactory.createCommand(commandName, agencyFactory, agencyRepository);
            List<String> parameters = commandParser.parseParameters(commandAsString);
            String executionResult = command.execute(parameters);
            writer.writeLine(executionResult);
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
    }

}