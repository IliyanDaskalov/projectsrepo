package com.telerikacademy.oop.agency.core.factories;

import com.telerikacademy.oop.agency.core.contracts.AgencyFactory;
import com.telerikacademy.oop.agency.models.contracts.Journey;
import com.telerikacademy.oop.agency.models.contracts.Ticket;
import com.telerikacademy.oop.agency.models.vehicles.contracts.Airplane;
import com.telerikacademy.oop.agency.models.vehicles.contracts.Bus;
import com.telerikacademy.oop.agency.models.vehicles.contracts.Train;
import com.telerikacademy.oop.agency.models.vehicles.contracts.Vehicle;

public class AgencyFactoryImpl implements AgencyFactory {
    
    public AgencyFactoryImpl() {
    }
    
    public Bus createBus(int passengerCapacity, double pricePerKilometer) {
        // We will be creating the new Bus Objects here. In order to do that we have to implement BusImpl first
        // and then we can return that BusImpl here with the proper parameters.
        throw new UnsupportedOperationException("Not implemented yet.");
    }
    
    public Airplane createAirplane(int passengerCapacity, double pricePerKilometer, boolean hasFreeFood) {
        throw new UnsupportedOperationException("Not implemented yet.");
    }
    
    public Train createTrain(int passengerCapacity, double pricePerKilometer, int carts) {
        throw new UnsupportedOperationException("Not implemented yet.");
    }
    
    public Journey createJourney(String startLocation, String destination, int distance, Vehicle vehicle) {
        throw new UnsupportedOperationException("Not implemented yet.");
    }
    
    public Ticket createTicket(Journey journey, double administrativeCosts) {
        throw new UnsupportedOperationException("Not implemented yet.");
    }
    
}