package com.telerikacademy.oop.agency.core;

import com.telerikacademy.oop.agency.core.contracts.AgencyRepository;
import com.telerikacademy.oop.agency.models.contracts.Journey;
import com.telerikacademy.oop.agency.models.contracts.Ticket;
import com.telerikacademy.oop.agency.models.vehicles.contracts.Vehicle;

import java.util.ArrayList;
import java.util.List;

public class AgencyRepositoryImpl implements AgencyRepository {

    private final List<Vehicle> vehicles = new ArrayList<>();
    private final List<Journey> journeys = new ArrayList<>();
    //TODO implement Ticket and add getter and add method

    public List<Vehicle> getVehicles() {
        return new ArrayList<>(vehicles);
    }

    public void addVehicle(Vehicle vehicle) {
        this.vehicles.add(vehicle);
    }

    public List<Journey> getJourneys() {
        return new ArrayList<>(journeys);
    }

    public void addJourney(Journey journey) {
        this.journeys.add(journey);
    }

    public List<Ticket> getTickets() {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public void addTicket(Ticket ticket) {
        throw new UnsupportedOperationException("Not implemented yet.");
    }
}