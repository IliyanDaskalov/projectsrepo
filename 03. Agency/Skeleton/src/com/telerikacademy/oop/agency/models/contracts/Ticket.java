package com.telerikacademy.oop.agency.models.contracts;

public interface Ticket {
    
    double getAdministrativeCosts();
    
    Journey getJourney();
    
    double calculatePrice();
    
}