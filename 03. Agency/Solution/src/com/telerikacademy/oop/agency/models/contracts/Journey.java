package com.telerikacademy.oop.agency.models.contracts;

import com.telerikacademy.oop.agency.models.vehicles.contracts.Vehicle;

public interface Journey {
    
    String getDestination();
    
    int getDistance();
    
    String getStartLocation();
    
    Vehicle getVehicle();
    
    double calculateTravelCosts();
    
}