package com.telerikacademy.oop.agency.models;

import com.telerikacademy.oop.agency.models.contracts.Journey;
import com.telerikacademy.oop.agency.models.contracts.Ticket;

public class TicketImpl implements Ticket {

    Journey journey;
    private double administrativeCosts;

    public TicketImpl(Journey journey, double administrativeCosts) {
        setJourney(journey);
        setAdministrativeCosts(administrativeCosts);
    }

    private void setJourney(Journey journey) {
        Validator.validateNotNull("Journey",journey);
        this.journey = journey;
    }

    private void setAdministrativeCosts(double administrativeCosts) {
        Validator.validatePrice(administrativeCosts,"Administrative costs cannot be negative.");
        this.administrativeCosts = administrativeCosts;
    }

    @Override
    public double getAdministrativeCosts() {
        return administrativeCosts;
    }

    @Override
    public Journey getJourney() {
        return journey;
    }

    @Override
    public double calculatePrice() {
        return administrativeCosts * (journey.calculateTravelCosts());
    }


    @Override
    public String toString() {
        return String.format("Ticket ----%n" +
                        "Destination: %s%n" +
                        "Price: %.2f%n",
                        journey.getDestination(),
                        calculatePrice());
    }
}
