package com.telerikacademy.oop.agency.commands.listing;

import com.telerikacademy.oop.agency.commands.contracts.Command;
import com.telerikacademy.oop.agency.core.contracts.AgencyRepository;
import com.telerikacademy.oop.agency.models.contracts.Journey;
import com.telerikacademy.oop.agency.models.vehicles.contracts.Vehicle;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.oop.agency.commands.CommandsConstants.JOIN_DELIMITER;

public class ListVehiclesCommand implements Command {
    // TODO
    private final List<Vehicle> vehicles;

    public ListVehiclesCommand(AgencyRepository agencyRepository) {
        vehicles = agencyRepository.getVehicles();
    }

    public String execute(List<String> parameters) {
        if (vehicles.size() == 0) {
            return "There are no registered vehicles.";
        }
        List<String> listVehicles = vehiclesToString();
        return String.join(JOIN_DELIMITER + System.lineSeparator(), listVehicles).trim();
    }

    private List<String> vehiclesToString() {
        List<String> stringifiedVehicles = new ArrayList<>();
        for (Vehicle vehicle : vehicles) {
            stringifiedVehicles.add(vehicle.toString());
        }
        return stringifiedVehicles;
    }
}